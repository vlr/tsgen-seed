import { getGitlabToken } from "./homeFiles";

const gitlabUrl = "https://gitlab.com/api/v4";

function fullUrl(url: string): string {
  return gitlabUrl + url;
}

async function run<T = any>(method: string, url: string, body: any): Promise<T> {
  const key = await getGitlabToken();
  const options = {
    method,
    headers: {
      "Private-Token": key
    },
    url: fullUrl(url),
    json: true,
    body
  };

  // tslint:disable-next-line:no-require-imports
  const req = require("request-promise-native");
  const result: T = await req(options);
  return result;
}

function post<T = any>(url: string, body: any): Promise<T> {
  return run("POST", url, body);
}

async function del<T = any>(url: string, body: any = {}): Promise<T> {
  return run("DELETE", url, body);
}

export const request = {
  post,
  del
};
