import { series } from "gulp";
import { build } from "../build/build";
import { runTests, removeTestDataFolder, buildTestData } from "./parts";
import { applyGenToTestData } from "./applyGenToTestData";

const prepareTestData = series(
  removeTestDataFolder,
  applyGenToTestData,
  buildTestData
);

export const e2eTest = series(
  build,
  prepareTestData,
  runTests,
);
