
export function combineStreams(streams: NodeJS.ReadWriteStream[]): Promise<void> {
  return <any>Promise.all(streams.map(streamToPromise));
}

function streamToPromise(stream: NodeJS.ReadWriteStream): Promise<void> {
  return new Promise(resolve => {
    stream.on("end", () => resolve());
  });
}
