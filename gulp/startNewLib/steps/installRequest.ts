import { spawnIt } from "@vlr/spawn";

export async function installRequest(): Promise<void> {
  await spawnIt("npm i", "npm", ["install", "request", "request-promise-native", "--save-dev"], {});
}
