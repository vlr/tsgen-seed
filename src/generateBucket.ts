import { ContentFile } from "@vlr/gulp-transform";
import { exportedTypes } from "@vlr/type-parser";
import { GeneratorOptions } from "./common/generatorOptions.type";
import { libMark } from "./common/libMark";
import { ParsedModel } from "./common/parsed.model";

export function generateBucket(folder: ParsedModel[], options: GeneratorOptions): ContentFile {
  if (hasForeignIndex(folder, options)) { return null; }
  const files = folder.filter(hasExports).map(x => x.file.name);
  if (files.length === 0) { return null; }
  return {
    ...folder[0].file,
    name: "index",
    contents: generateBucketContent(files, options)
  };
}

function generateBucketContent(files: string[], options: GeneratorOptions): string {
  const exportLines = files.map(file => `export * from ${options.quotes}./${file}${options.quotes};${options.linefeed}`);
  return libMark(options) + options.linefeed + exportLines.join("");
}

function hasForeignIndex(folder: ParsedModel[], options: GeneratorOptions): boolean {
  const index = folder.find(x => x.file.name === "index");
  return index != null && index.file.contents.includes(libMark(options));
}

function hasExports(parsed: ParsedModel): boolean {
  return exportedTypes(parsed).length > 0;
}
