import { Version } from "./version";
import * as fse from "fs-extra";

const pkgFile = "package.json";

const regex = /"version": "(\d\d*).(\d\d*).(\d\d*)"/;
export async function getNpmVersion(): Promise<Version> {
  const pkg = await readPkgFile();
  const m = pkg.match(regex);
  return {
    major: +m[1],
    minor: +m[2],
    patch: +m[3]
  };
}

async function readPkgFile(): Promise<string> {
  return await fse.readFile("package.json", "utf8");
}

export async function applyNpmVersion(version: Version): Promise<void> {
  const pkg = await readPkgFile();
  const replaced = pkg.replace(regex, `"version": "${version.major}.${version.minor}.${version.patch}"`);
  await fse.writeFile(pkgFile, replaced);
}
