import { removeDir } from "../../tools/removeDir";
import { testDataFolder } from "../../settings";

export function removeTestDataFolder(): Promise<void> {
  return removeDir(testDataFolder());
}
