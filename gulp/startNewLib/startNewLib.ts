import { series } from "gulp";
import {
  changeRemote, cloneSeedToNewLibDir, createBranch, createRepoOnGitLab, ensureFilesExist,
  gitReset, goBackToSeed, goIntoLibDir, goOneDirUp, installNodeModules, installRequest,
  pushChangesToBranch, pushMaster, replaceLibName, setMasterProtection, setRepoEnvVariables
} from "./steps";

export const startNewLib = series(
  ensureFilesExist,
  installRequest,
  createRepoOnGitLab,
  setRepoEnvVariables,
  goOneDirUp,
  cloneSeedToNewLibDir,
  goIntoLibDir,
  installNodeModules,
  changeRemote,
  pushMaster,
  setMasterProtection,
  createBranch,
  replaceLibName,
  pushChangesToBranch,
  goBackToSeed,
  gitReset
);
