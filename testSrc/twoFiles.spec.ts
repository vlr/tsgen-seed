import { expect } from "chai";

// tslint:disable: no-require-imports
const target = require("../testData/twoFiles");

describe("one file", function (): void {
  it("should import both funcs from bucket", function (): void {
    // arrange

    // act
    const result1 = target.func1();
    const result2 = target.func2();

    // assert
    expect(result1).equals(1);
    expect(result2).equals(2);
  });
});
