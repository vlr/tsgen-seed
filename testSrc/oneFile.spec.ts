import { expect } from "chai";

// tslint:disable: no-require-imports
const target = require("../testData/oneFile");

describe("one file", function (): void {
  it("should import func from bucket", function (): void {
    // arrange

    // act
    const result = target.func();

    // assert
    expect(result).equals(1);
  });
});
