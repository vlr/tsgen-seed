# tsgen-seed
This is a seed for typescript generator, includes basic amenities like build an lint from tslib-seed, except building and testing with es5 and es6 targets. This is because generators are not supposed to be used in live projects, only during design time.

# Features and usage
Along with basic features, this seed adds E2E testing feature. Task e2eTest is created for that purpose. It builds the main project, then applies generator onto the testData, then tests are executed.
You might need to update the require statement in runGenerator.ts file.


# Creating a typescript library from this seed
First, in your 'users/{username}/.ssh' folder, create files 'gitlab' and 'npm' with access tokens allowing to access gitlab api and publish npm packages respectively
Then, in folder of cloned tsgen-seed run "gulp startNewLib -n {gitlab_login}/{repository_name}"
This creates a repository on gitlab populated from this seed and feature/first-implementation branch