import * as fse from "fs-extra";
import { spawnIt } from "@vlr/spawn";
import { projectPath } from "./projectName";
import { parallel } from "gulp";

export const replaceLibName = parallel(
  replaceRemoteInLibMark,
  replaceRemoteInPackage,
  replaceRemoteInGitlabCi
);

export function replaceRemoteInLibMark(): Promise<void> {
  return replaceInFile("src/common/libMark.ts", replaceLibraryName);
}

export function replaceRemoteInPackage(): Promise<void> {
  return replaceInFile("package.json", replaceLibraryName);
}

export function replaceRemoteInGitlabCi(): Promise<void> {
  return replaceInFile(".gitlab-ci.yml", replaceLibraryName);
}

function replaceLibraryName(content: string): string {
  return content.replace(/vlr\/tsgen-seed/g, projectPath());
}

async function replaceInFile(file: string, replacer: (f: string) => string): Promise<void> {
  const content = await fse.readFile(file, "utf8");
  const result = replacer(content);
  await fse.writeFile(file, result);
}

export async function installNodeModules(): Promise<void> {
  await spawnIt("npm i", "npm", ["i"], {});
}
