import { ParserOptions } from "@vlr/type-parser";
import { GeneratorOptions as BaseOptions } from "@vlr/razor/export";

export interface GeneratorOptions extends ParserOptions, BaseOptions { }
