import * as os from "os";
import * as fse from "fs-extra";

const gitlabToken = "gitlab";
const npmToken = "npm";
const gitPrivateKey = "id_rsa";

export async function ensureFilesExist(): Promise<void> {
  await ensureFile(gitPrivateKey, "private git key");
  await ensureFile(npmToken, "npm access token");
  await ensureFile(gitlabToken, "gitlab access token");
}

async function ensureFile(file: string, message: string): Promise<void> {
  if (!await fse.pathExists(sshDir() + file)) {
    throw new Error(`File ${file}, ${message} does not exist`);
  }
}

function sshDir(): string {
  return os.homedir() + "/.ssh/";
}

export function getGitlabToken(): Promise<string> {
  return fse.readFile(sshDir() + gitlabToken, "utf8");
}

export function getNpmToken(): Promise<string> {
  return fse.readFile(sshDir() + npmToken, "utf8");
}

export function getGitPrivateKey(): Promise<string> {
  return fse.readFile(sshDir() + gitPrivateKey, "utf8");
}
